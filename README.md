# Investigate the upper bound of modification of the ANI when plasmids are shared

* Computation of the expected variation in ANI values when sharing an identical plasmid sequence between two genomes
* Verification with fastANI and artificial introduction of a plasmid sequence

## Expected ANI variations

The R script and the associated figure shows the range of genome and plasmid length at which we could expect significatn ANI changes.

## Verification with fastANI

```bash 
cp -t . ~/projects/bacterial-collection/hibc-genomes/temp-share-point/HiBC_genomes/CLAAAH{180,266}.genome.fa.gz
rsync -arv winogradsky:~/projects/bacterial-collection/20220902_hibc/results/plasmids/CLAAAH266.plasmids.fa.gz .
# Add the plasmid sequence ~4.7kb
zcat CLAAAH266.genome.fa.gz CLAAAH266.plasmids.fa.gz |gzip > CLAAAH266.combined.fasta.gz
fastANI -q CLAAAH180.genome.fa.gz -r CLAAAH266.genome.fa.gz -o genomes-180-vs-266
fastANI -q CLAAAH180.genome.fa.gz -r CLAAAH266.combined.fasta.gz -o genomes-180-vs-266-with-plasmid
# both are 95.4025

# Create an artifical plasmid exchange
zcat CLAAAH180.genome.fa.gz CLAAAH266.plasmids.fa.gz |gzip > CLAAAH180.with-plasmid266.fasta.gz
fastANI -q CLAAAH180.with-plasmid266.fasta.gz -r CLAAAH266.combined.fasta.gz -o genomes-180-with-plasmid-vs-266-with-plasmid
# 95.4091 So no crazy change 
```


# Literature

* lengths curated resource for plasmid (mostly clinical isolates?): https://ccb-microbe.cs.uni-saarland.de/plsdb/plasmids/
* using the NCBI directly: https://www.ncbi.nlm.nih.gov/genome/browse#!/plasmids/
* approach for reconstructing plasmids acc to the CAMIII: https://doi.org/10.1016/j.plasmid.2021.102576
